<?php


class PageData extends CmsModel{
	protected static $cacheDir = "cms.pages.data";
	protected static $tableName = "cms_pages_data";

	public $id,$page,$box,$object,$params,$poradie,$show,$options,$lang;

	function __construct() {
		if ( func_num_args() >= 1 ) {
			$id=func_get_arg(0);

			if ( Cache::check( static::$cacheDir, $id ) ) {
				$R = Cache::get( static::$cacheDir, $id );
			} else {
				if ( is_numeric($id) ) {
					$db      = Registry::get( "database" );
					$q = $db->query( "select * from " . "cms_pages_data" . " where id=?", (int) $id );
					$f = $q->fetch();
					if($f) {
						$R = $f;
						Cache::put( static::$cacheDir, $id, $R );
					}
				};
			};

			if ( isset( $R ) ) {
				foreach($R as $k=>$v){
					$this->{$k} = $v;
				}
				$this->params = unserialize($this->params);
				$this->options = unserialize($this->options);
			}

		};

	}

	public function hidden(){
		if($this->show == "hide"){
			return true;
		}
		return false;
	}
	public function showHide(){
		//dump($_SERVER);
		if($this->hidden()){
			return '<a href="?action=pagedatashow&id='.$this->id.'&uri='.$_SERVER["SCRIPT_NAME"].'&uri_action='.$_GET["action"].'&uri_id='.$_GET["id"].'" title="Odkryť"><img src="img/rse/btn_no.gif" width="20"></a>';
		}
		return '<a href="?action=pagedatahide&id='.$this->id.'&uri='.$_SERVER["SCRIPT_NAME"].'&uri_action='.$_GET["action"].'&uri_id='.$_GET["id"].'" title="Skryť"><img src="img/rse/btn_ok.gif" width="20"></a>';
	}
	public function showMove(){
		$ret = "";
		if($this->isMovableUp()){
			$ret .= '<a href="?action=pagedatamoveup&id='.$this->id.'&uri='.$_SERVER["SCRIPT_NAME"].'&uri_action='.$_GET["action"].'&uri_id='.$_GET["id"].'"><img src="img/rse/move_up.gif" width="20"></a>';
		}
		if($this->isMovableDown()){
			$ret .= '<a href="?action=pagedatamovedown&id='.$this->id.'&uri='.$_SERVER["SCRIPT_NAME"].'&uri_action='.$_GET["action"].'&uri_id='.$_GET["id"].'"><img src="img/rse/move_down.gif" width="20"></a>';
		}
		return $ret;

	}
	public function showEdit(){
		return '<a href="?action=pagedataedit&id='.$this->id.'&uri='.$_SERVER["SCRIPT_NAME"].'&uri_action='.$_GET["action"].'&uri_id='.$_GET["id"].'"><img src="img/rse/btn_zoom.gif" width="20"></a>';
	}
	public function showDelete(){
		return '<a href="?action=pagedatadelete&id='.$this->id.'&uri='.$_SERVER["SCRIPT_NAME"].'&uri_action='.$_GET["action"].'&uri_id='.$_GET["id"].'"><img src="img/rse/btn_cancel.gif" width="20"></a>';
	}

	public function isMovableUp(){
		return !!$this->getPrev()->id;
	}
	public function isMovableDown(){
		return !!$this->getNext()->id;
	}
	private function getPrev(){
		$ret=new self;
		$db = Registry::get("database");
		$r = $db->query("SELECT id FROM ".self::$tableName." WHERE poradie < ? and page = ? and box = ? ORDER BY poradie DESC LIMIT 1",$this->poradie,$this->page,$this->box)->fetch();
		if($r){
			$ret=new self($r["id"]);
		};
		return $ret;
	}

	private function getNext(){
		$ret=new self;
		$db = Registry::get("database");
		$r = $db->query("SELECT id FROM ".self::$tableName." WHERE poradie > ? and page = ? and box = ? ORDER BY poradie ASC LIMIT 1",$this->poradie,$this->page,$this->box)->fetch();
		if($r){
			$ret=new self($r['id']);
		};
		return $ret;
	}

	private function swapOrder($pair){
		$tmp=$this->poradie;
		$this->poradie=$pair->poradie;
		$pair->poradie=$tmp;
		$this->save();
		$pair->save();
	}
	public function moveTop(){
		if($this->isMovableUp()){
			$R = $this->db->query("select min(poradie) as poradie from ".self::$tableName." WHERE page = ? and box = ?",$this->page,$this->box)->fetch();
			$this->poradie=($R['poradie']-1);
			$this->save();
		};
	}
	public function moveBottom(){
		if($this->isMovableDown()){
			$R = $this->db->query("select max(poradie) as poradie from ".self::$tableName." WHERE page = ? and box = ?",$this->page,$this->box)->fetch();
			$this->poradie=($R['poradie']+1);
			$this->save();
		};
	}
	public function moveUp(){
		if($this->isMovableUp()){
			$this->swapOrder($this->getPrev());
		};
	}
	public function moveDown(){
		if($this->isMovableDown()){
			$this->swapOrder($this->getNext());
		};
	}
	public function showLanguages(){
		$ret["all"] = '<a style="'.(($this->lang === null || trim($this->lang) == "")?'font-weight:bold;':'opacity:0.5').'" href="?action=pagedatalang&id='.$this->id.'&lang=all&uri='.$_SERVER["SCRIPT_NAME"].'&uri_action='.$_GET["action"].'&uri_id='.$_GET["id"].'">Všetky jazyky</a>';
		foreach(Config::$publicLanguages as $lg){
			$ret[$lg] = '<a style="'.(($this->lang == $lg)?'font-weight:bold;':'opacity:0.5').'" href="?action=pagedatalang&id='.$this->id.'&lang='.$lg.'&uri='.$_SERVER["SCRIPT_NAME"].'&uri_action='.$_GET["action"].'&uri_id='.$_GET["id"].'">'.\Nette\Utils\Strings::upper($lg).'</a>';
		}
		return implode( " | " ,$ret);
	}
}