<?php


class Page extends CmsModel{
	private $db;
	private static $instances = [];
	protected static $cacheDir = "cms.pages";
	protected static $tableName = "cms_pages";
	/** @var Template|null  */
	private $templateObject = null;

	public $id,$template,$title,$desc,$keywords,$url,$url_cz,$robots,$microdata,$image,$mobile,$main,$system;


	function __construct() {
		$this->db = $db = Registry::get( "database" );
		if ( func_num_args() >= 1 ) {
			$id=func_get_arg(0);
			if(isset(static::$instances[$id])){
				$R = static::$instances[$id];
			} else {
				if ( Cache::check( static::$cacheDir, $id ) ) {
					$R = Cache::get( static::$cacheDir, $id );
				} else {
					if ( is_numeric( $id ) ) {

						$q  = $db->query( "select * from " . "cms_pages" . " where id=?", (int) $id );
						$f  = $q->fetch();
						if ( $f ) {
							$R = $f;
							static::$instances[$id] = $R;
							Cache::put( static::$cacheDir, $id, $R );
						}
					};
				};
			}

			if ( isset( $R ) ) {
				foreach($R as $k=>$v){
					$this->{$k} = $v;
				}
				$this->templateObject = new Template($this->template);
			}

		};

	}
	public function isMain(){
		return (bool) $this->main;
	}

	/**
	 * @return null|Template class
	 */
	public function template(){
		return $this->templateObject;
	}
	public static function getMain(){
		/*$Q = mysqli_query( CommerceDB::$DB,
			"select id from cms_pages where main=1");
		$r = mysqli_fetch_assoc($Q);*/
		$db = Registry::get("database");
		$R = $db->query("select id from cms_pages where main=1")->fetch();
		return new self($R["id"]);
	}
	public static function getBySeolink($seolink){
		$db = Registry::get("database");
		if ( Cache::check( "cms.pages", $seolink ) ) {
			return new self(Cache::get( "cms.pages", $seolink ));
		} else {
			/*$Q = mysqli_query( CommerceDB::$DB,
				"select id from cms_pages where (url='" . mysqli_real_escape_string(CommerceDB::$DB,$seolink) . "' or url_cz = '" . mysqli_real_escape_string(CommerceDB::$DB,$seolink) . "')" );*/
			$R = $db->query("select id from cms_pages where (url=? or url_cz = ?)",$seolink,$seolink)->fetch();
			if ( $R ) {
				$self = new self($R["id"]);;
				Cache::put( "cms.pages", $seolink, $R["id"] );
				return $self;
			};
			return new self();
		}

	}
	public function data($params=[],$order = "poradie ASC"){
		$ret=[];
		$hash = "_list_".$this->id."_".md5(serialize($params));
		if(Cache::check(static::$cacheDir.".data",$hash)){ $ret=Cache::get(static::$cacheDir.".data",$hash); } else {
			/*$param_query = "true";
			if(!empty($params)){
				foreach($params as $k=>$v){
					$param_query = $k."='".$v."' and";
				}
				$param_query = substr($param_query,0,-3);
			}*/
			$params["page"] = $this->id;
			//$Q=mysqli_query(CommerceDB::$DB,"select id from cms_pages_data WHERE page='".$this->id."' and ".$param_query." ORDER BY ".$order);
			//while($R=mysqli_fetch_assoc($Q)){
			$q = $this->db->query("select id from cms_pages_data WHERE",$params," ORDER BY ".$order);
			foreach($q as $R){
				$ret[]=new PageData($R['id']);
			};
			Cache::put(static::$cacheDir.".data",$hash,$ret);
		};
		return $ret;
	}
}