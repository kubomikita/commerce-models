<?php


class Template extends CmsModel{
	private static $instances = [];
	private $db;
	//private $cacheDir = "cms.templates";
	protected static $cacheDir = "cms.templates";
	protected static $tableName = "cms_templates";

	public $id;
	public $nazov;
	public $popis;
	public $src;
	public $main;

	public static function fetchSelect() {
		$select = [];
		foreach (self::fetch() as $template){
			$select[$template->id] = $template->nazov;
		}
		return $select;
	}

	function __construct(){
		$this->db = $db = Registry::get( "database" );
		if(func_num_args()>=1){
			$id=(int)func_get_arg(0);
			if($id==0){ return true; };
			if(isset(static::$instances[$id])){
				$R = static::$instances[$id];
			} else {
				if ( Cache::check( static::$cacheDir, $id ) ) {
					$R = Cache::get( static::$cacheDir, $id );
				} else {

					$q  = $db->query( "select * from " . "cms_templates" . " where id=?", (int) $id );
					$f  = $q->fetch();

					if ( $f ) {
						$R = $f;
						static::$instances[$id] = $R;
						Cache::put( static::$cacheDir, $id, $R );
					};
				};
			}

			if(isset($R)){
				$this->id=$R['id'];
				$this->nazov=$R['nazov'];
				$this->popis=$R['popis'];
				$this->src=$R['src'];
				$this->main = $R["main"];
			}

		};
	}

	public function data($params=[],$order = "poradie ASC"){
		$ret=[];
		$id = $this->id;
		$instanceid = "datanum_".$id;
		if(isset(static::$instances[$instanceid])){
			$count = static::$instances[$instanceid];
		} else {
			$num = $this->db->query( "SELECT count(id) as `count` FROM cms_templates_data WHERE template = ?",
				$id )->fetch();
			$count = $num["count"];
			static::$instances[$instanceid] = $count;
		}
		if ( ! $count) {
			$main = self::getMain();
			$id   = $main->id;
			/*foreach ($this->boxes() as $box){
				$td = new TemplateData();
				$td->box = $box->id;
				$td->template = $this->id;
				$ret[] = $td;
			}*/
		}

		$hash = "_list_".$id."_".md5(serialize($params));
		if(Cache::check(static::$cacheDir,$hash)){ $ret=Cache::get(static::$cacheDir,$hash); } else {
			$params["template"] = $id;
			$q = $this->db->query("select * from cms_templates_data WHERE",$params," ORDER BY ".$order);
			foreach($q as $R){
				//dump($R);
				$ret[]=new TemplateData($R['id']);
			};
			Cache::put(static::$cacheDir,$hash,$ret);
		};
		//dump($ret,$params,$id);exit;
		//dump($ret);
		return $ret;
	}

	public static function getMain(){
		$hash = "main";
		if(isset(static::$instances["mainid"])){
			$ret = static::$instances["mainid"];
		} else {
			if ( Cache::check( static::$cacheDir, $hash ) ) {
				$ret = Cache::get( static::$cacheDir, $hash );
			} else {
				$db  = Registry::get( "database" );
				$q   = $db->query( "SELECT * FROM cms_templates WHERE main = 1" )->fetch();
				$ret = static::$instances["mainid"] = $q->id;
				//bdump(debug_backtrace());
				Cache::put( static::$cacheDir, $hash, $ret );
			}
		}
		return new self( $ret );
	}

	public function boxes(){
		$ret=[];

		$id = $this->id;
		$instanceid = "boxesnum_".$id;
		if(isset(static::$instances[$instanceid])){
			$count = static::$instances[$instanceid];
		} else {
			$num = $this->db->query( "SELECT count(id) as `count` FROM cms_templates_boxes WHERE template = ?",
				$id )->fetch();
			$count = $num["count"];
			static::$instances[$instanceid] = $count;
		}
		if ( ! $count) {
			$main = self::getMain();
			$id   = $main->id;
		}

		$hash = "_boxes_".$id;
		if(Cache::check(static::$cacheDir,$hash)){ $ret=Cache::get(static::$cacheDir,$hash); } else {
			$q = $this->db->query("select ident,nazov,boxid from cms_templates_boxes WHERE template=?  ORDER BY id ASC",$id);
			foreach($q as $R){
				/*$Q=mysqli_query(CommerceDB::$DB,"select ident,nazov,boxid from cms_templates_boxes WHERE template='".$this->id."'  ORDER BY id ASC");
				while($R=mysqli_fetch_assoc($Q)){*/
				$ret[$R["ident"]]["info"]=$R["nazov"];
				if(trim($R["boxid"]) != "") {$ret[$R["ident"]]["boxid"]=$R["boxid"];}
			};
			Cache::put(static::$cacheDir,$hash,$ret);
		};
		return $ret;
	}
	public function fullBoxes($Page,$OBJ){
		foreach($this->boxes() as $box=>$data){
			$data = $this->data(["box"=>$box]);

			foreach($data as $td){
				$pagedata["pageobject"] = $Page;
				$pagedata['pageid']=$Page->id;
				$pagedata['boxid']=$Page->id.'/'.$td->id;
				if(isset($pagedata["pager"])){
					$pagedata['pager']=(($_SESSION['pager'][$pagedata['boxid']]>0)?$_SESSION['pager'][$pagedata['boxid']]:1);
				}
				if(lock_check($td->show,$td->options) && lang_check($td->lang)){
					if($td->object =="var" and $td->id == 75){
						//continue;
					}
					if(isset($OBJ[$td->object])){
						/** @var CommerceController $OBJECT */
						$OBJECT = $OBJ[$td->object];
						Tracy\Debugger::timer("RENDER ".$OBJECT->objName);

						$OBJECT->setObjects($OBJ);

						if(!((isset($OBJECT->commerce) or isset($OBJECT->rse)) && $OBJECT->isLatteSupported($td->params)) && \Registry::get("container")->getParameter("latte","strict")){
							$OBJECT_RENDER = "nie latte<br>";
						} else {
							if($box == 3) {
								//dump( $data, $OBJ );
								//dump($OBJECT, $pagedata, $td->params);
							}
							$OBJECT_RENDER = $OBJECT->objRender($pagedata,$td->params);
						}
						$time = Tracy\Debugger::timer("RENDER ".$OBJECT->objName);
					} else {
						$OBJECT_RENDER = "zobrazovací modul neexistuje<br>";
					}

					//bdump($time,"Render ".$OBJECT->objName);
					//bdump($td);
					if(!isset($BOX[$box])){
						$BOX[$box] = "";
					}
					$BOX[$box] .= $OBJECT_RENDER;
				};
			}

		}
		return $BOX;
	}
	public function variables(){
		$ret=[];
		$hash = "_variables_".$this->id;
		if(Cache::check(static::$cacheDir,$hash)){ $ret=Cache::get(static::$cacheDir,$hash); } else {
			$q = $this->db->query("select id from cms_templates_data WHERE template=? and object = 'var'  ORDER BY box ASC, poradie ASC",$this->id);
			//$Q=mysqli_query(CommerceDB::$DB,"select id from cms_templates_data WHERE template='".$this->id."' and object = 'var'  ORDER BY box ASC, poradie ASC");
			//while($R=mysqli_fetch_assoc($Q)){
			foreach($q as $R){
				$ret[] = new TemplateData($R["id"]);
				//$ret[$R["ident"]]["info"]=$R["nazov"];
				//if(trim($R["boxid"]) != "") {$ret[$R["ident"]]["boxid"]=$R["boxid"];}
			};
			Cache::put(static::$cacheDir,$hash,$ret);
		};
		return $ret;
	}


}