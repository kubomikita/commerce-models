<?php

abstract class CmsModel {
	protected static $cacheDir = __CLASS__;
	protected static $tableName = null;

	public function __sleep(){
		//echo " Going to sleep, pdo = null";
		//$this->NotORM = null;
		//$this->PDO = null;
		$array=array();
		foreach($this as $k=>$v){
			if($k != "PDO" and $k!= "NotORM"){
				$array[]=$k;
			}
		}
		//var_dump($this,$array);
		return $array;
	}

	public function __wakeup(){
		//$this->connect();
	}

	public static function fetch($params = [], $order = "id DESC"){
		$params["ORDERBY"] = $order;
		$cacheDir = static::$cacheDir;
		$ret=array();
		$hash = "_list_".md5(serialize($params));
		unset($params["ORDERBY"]);

		return Cache::load($cacheDir, $hash, function (&$dependencies) use($params, $order){
			$ret = [];
			/** @var \Nette\Database\Connection $db */
			$db = \Kubomikita\Service::getByType(\Nette\Database\Connection::class);
			foreach($db->query('SELECT * FROM '.static::$tableName.' WHERE',$params,"ORDER BY ".$order) as $row) {
				$ret[]=new static($row->id);
			}
			return $ret;
		});

		//if(Cache::check($cacheDir,$hash)){ $ret=Cache::get($cacheDir,$hash); } else {

		if(!empty($params)){
			$param_query = "";
			foreach($params as $k=>$v){
				if(is_array($v)){
					$values = array_map(function($val){
						return "'".mysqli_real_escape_string(CommerceDB::$DB,$val)."'";
					},$v);
				}

				if(strpos($k,"NOT") !== false){
					$k = trim(str_replace("NOT","",$k));
					if(!empty($values)){
						$param_query .= "`".$k."` NOT IN (".implode(",",$values).") and";
					} else {
						$param_query .= "`".$k."` != '".mysqli_real_escape_string(CommerceDB::$DB,$v)."' and";
					}

				} else {
					if(!empty($values)){
						$param_query .= "`".$k."` IN (".implode(",",$values).") and";
					} else {
						$param_query .= "`" . $k . "` = '" . mysqli_real_escape_string( CommerceDB::$DB,$v ) . "' and";
					}
				}

			}
			bdump($param_query);
			//echo $param_query."<br>";
			$param_query = substr($param_query,0,-3);
		} else {
			$param_query = "true";
		}
		$qry = "select id from ".static::$tableName." WHERE ".$param_query." ORDER BY ".$order;
		bdump($qry);
		//echo $qry."<br>";
		$Q=mysqli_query(CommerceDB::$DB,$qry);

		while($R=mysqli_fetch_assoc($Q)){
			$ret[]=new static($R['id']);
		};
		Cache::put($cacheDir,$hash,$ret);
		//	};
		return $ret;
	}
	public function save(){
		//dump();
		$public = get_object_vars($this);
		unset($public["id"]);
		$update = [];
		if(!$this->id){ $this->id=mysqli_new(static::$tableName); };
		foreach($public as $k=>$v){
			if(is_array($v)){
				$v = serialize($v);
			}
			$update[] = "`$k` = '".addslashes($v)."'";
		}
		//dump($update);
		/* @var $this->nazov LangStr */
		mysqli_query(CommerceDB::$DB,"update ".static::$tableName." set ".implode(",",$update)." where id=".$this->id);
		//echo mysqli_error(CommerceDB::$DB);
		//echo("update ".static::$tableName." set ".implode(",",$update)." where id=".$this->id);
		Cache::flush(static::$cacheDir);
		Cache::flush(static::$cacheDir.".data");
	}
	public function delete(){
		$db = Registry::get("database");
		$db->query("DELETE FROM ".static::$tableName." WHERE id = ?",$this->id);
		Cache::flush(static::$cacheDir);
	}
}