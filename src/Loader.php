<?php


namespace Kubomikita\Commerce\Model;



use Nette\Loaders\RobotLoader;
use ReflectionClass;

class Loader {

	protected $loaded = [];

	public function load() {
		$this->loaded = \Cache::load("application.models","loaded", function (&$dependencies) {
			return $this->rebuild();
		});
		foreach ($this->loaded as $alias => $original) {
			class_alias($original, $alias);
		}
	}

	protected function rebuild() : array {
		$loader = new RobotLoader;
		$loader->addDirectory(__DIR__."/commerce");
		$loader->rebuild();
		$loaded = [];
		foreach ($loader->getIndexedClasses() as $class => $file){
			$reflection = new ReflectionClass($class);
			if($reflection->getParentClass() instanceof ReflectionClass && $reflection->getParentClass()->getName() === \ActiveRecord::class) {
				$classes = [
					"\\".str_replace("Model","", $reflection->getShortName()),
					"\\".$reflection->getShortName(),
				];
				foreach ($classes as $className) {
					if ( ! class_exists( $className ) ) {
						//class_alias( $reflection->getName(), $className );
						$loaded[ $className ] = $reflection->getName(); //['class' => $reflection->getName(), 'file' => $file];
					} else {
						$ref                        = new ReflectionClass( $className );
						$loaded[ $className ] = $ref->getName(); //['class' => $ref->getName(), 'file' => $ref->getFileName()];
					}
				}

			}

		}
		return $loaded;
	}

}