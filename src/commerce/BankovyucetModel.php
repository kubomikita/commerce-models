<?php
namespace Kubomikita\Commerce\Model;

use ActiveRecord;

class Bankovyucet extends ActiveRecord{
	protected static $table="ec_bankoveucty";
	public $id;
	public $popis;
	public $nazov_banky;
	public $cislo_uctu;
	public $kod_banky;
	public $iban;
	public $swift;
	/**
	 * @global $visible
	 * @write true
	 */
	public $visible = 1;

	public function deletable(){
		return !!$this->id;
	}

	public static function fetch(){
		$ret=array();
		$db=\Registry::get("database");
		$q=$db->query("SELECT * FROM ".static::$table);
		foreach($q as $R){
			$ret[]=new Bankovyucet($R);
		}
		return $ret;
	}

	public static function fetchVisibleOnly(){
		$ret=array();
		foreach(Bankovyucet::fetch() as $BU){
			if($BU->visible){ $ret[]=$BU; };
		};
		return $ret;
	}

	public static function fetchSelectbox(){
		$ret=array();
		foreach(Bankovyucet::fetch() as $BU){
			$ret[$BU->id]=$BU->nazov;
		};
		return $ret;
	}

	public static function fetchSelectboxVisibleOnly(){
		$ret=array();
		foreach(Bankovyucet::fetchVisibleOnly() as $BU){
			$ret[$BU->id]=$BU->nazov;
		};
		return $ret;
	}


}
