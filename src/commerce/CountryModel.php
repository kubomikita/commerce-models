<?php

namespace Kubomikita\Commerce\Model;

use ActiveRecord;

class Country extends ActiveRecord{
	protected static $table = "ec_countries";
	public $id;
	public $skratka;
	/** @var LangStr */
	public $nazov;
	public $currency = \Config::nativeCurrency;
	/** @var serialized */
	public $metadata;
	public $eu = 1;
	public $visible = 1;
	public $predvolba;
	public $pohoda_id;

	public function deletable(){
		$q =  $this->db->query("select id from ec_partneri where fa_country=?",$this->id);
		$count = $q->getRowCount();
		if($count){
			return false;
		}
		return true;
	}

	public static function fetch($where = []){
		return static::fetchAll($where);
	}

	public static function fetchVisibleOnly(){
		$ret=array();
		foreach(self::fetch(["visible" => 1]) as $C){
			$ret[]=$C;
		}
		return $ret;
	}

	public static function fetchSelectbox(){
		$ret=array();
		foreach(self::fetch() as $C){
			$ret[$C->id]=$C->nazov;
		};
		return $ret;
	}

	public static function fetchSelectboxVisibleOnly(){
		$ret=array();
		foreach(self::fetchVisibleOnly() as $C){
			$ret[$C->id]=$C->nazov;
		};
		return $ret;
	}


}

