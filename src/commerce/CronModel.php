<?php

namespace Kubomikita\Commerce\Model;

class CronModel extends \ActiveRecord {
	protected static $table = "ec_cron";
	/** @var int */
	public $id;
	/** @var string */
	public $name;
	/** @var int */
	public $enabled;
	/** @var string */
	public $model;
	/** @var json */
	public $args;
	/** @var string */
	public $minute;
	/** @var datetime */
	public $last;
}