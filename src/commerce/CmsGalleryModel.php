<?php
namespace Kubomikita\Commerce\Model;

use ActiveRecord;

class CmsGalleryModel extends ActiveRecord {
	public static $table = "cms_img_galerie";
	public static $table_images = "cms_img_images";
	public $id;
	public $nazov;
	public $popis;

	public function nazov(){
		return $this->nazov;
	}
}